package com.examalbo.alboexamen.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.examalbo.alboexamen.R
import com.examalbo.alboexamen.common.Navigation
import com.examalbo.alboexamen.network.model.BeerResponse
import com.examalbo.alboexamen.network.model.BeerResponseAItem
import com.squareup.picasso.Picasso

class BeersAdapter(private var items: BeerResponse, internal var itemClick: Navigation?) :RecyclerView.Adapter<BeersAdapter.HolderViewBeer>() {

    private var context: Context? = null
    private val picasso = Picasso.get()
    private var intemClicks =itemClick





    override fun getItemCount(): Int {
     return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderViewBeer {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view, parent, false)

        return HolderViewBeer(v,this)

    }

    override fun onBindViewHolder(holder: HolderViewBeer, position: Int) {
       holder.nameBeer.text = items.get(position).name
        holder.tagLine.text = items.get(position).tagline
        picasso.load(items.get(position).image_url).into(holder.image)
    }
     fun updateList(beerResponse: BeerResponse){
         this.items = beerResponse
     }


    class HolderViewBeer(itemView:View,private val beersAdapter: BeersAdapter):RecyclerView.ViewHolder(itemView),View.OnClickListener{
        val image : ImageView
        val nameBeer : TextView
        val  tagLine : TextView
        var beerResponse: BeerResponseAItem? = null
        init {

            image = itemView.findViewById(R.id.imageBeer)
            nameBeer = itemView.findViewById(R.id.nameBeer)
            tagLine = itemView.findViewById(R.id.tagLine)
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            beerResponse = this.beersAdapter.items.get(adapterPosition)
           beersAdapter.intemClicks?.nextScreen(this, beerResponse!!)
        }


    }

}

