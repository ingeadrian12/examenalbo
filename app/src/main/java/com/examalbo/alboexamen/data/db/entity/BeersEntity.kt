package com.examalbo.alboexamen.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.examalbo.alboexamen.network.model.BeerResponse

@Entity(tableName = "beers")
data class BeersEntity (var name:String,var tagline:String,var urlimage:String){
    @PrimaryKey(autoGenerate = true)
    var id = 0
}