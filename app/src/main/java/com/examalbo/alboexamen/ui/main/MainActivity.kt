package com.examalbo.alboexamen.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.examalbo.alboexamen.App
import com.examalbo.alboexamen.R
import com.examalbo.alboexamen.common.BaseActivity
import com.examalbo.alboexamen.network.model.BeerResponse
import com.examalbo.alboexamen.ui.adapters.BeersAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    private val mNavigation by lazy { findNavController(R.id.nav_graph) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

       toolbar.title = "Albo Adrian"

    }


}