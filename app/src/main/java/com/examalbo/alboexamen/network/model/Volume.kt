package com.examalbo.alboexamen.network.model

data class Volume(
    val unit: String,
    val value: Float
)