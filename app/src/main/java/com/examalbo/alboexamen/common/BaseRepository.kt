package com.examalbo.alboexamen.common

import androidx.lifecycle.LiveData
import com.examalbo.alboexamen.App
import com.examalbo.alboexamen.BuildConfig
import com.examalbo.alboexamen.data.db.DataBaseBeers.Companion.getDatabase
import com.examalbo.alboexamen.data.db.dao.BeersDao
import com.examalbo.alboexamen.data.db.entity.BeersEntity
import com.examalbo.alboexamen.network.networkmanager.ManagerNetwork

open class BaseRepository {
    val application = App.instance
    var all: LiveData<List<BeersEntity?>?>? = null
    var services = ManagerNetwork.getBeersApi(BuildConfig.BASE_URL)
     val beersDao: BeersDao?
    init {
         val db = getDatabase(application)
           beersDao = db!!.beersDao()
            all.apply {
              all = beersDao?.allBeers
            }

    }


}