package com.examalbo.alboexamen.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.examalbo.alboexamen.App
import com.examalbo.alboexamen.data.db.dao.BeersDao
import com.examalbo.alboexamen.data.db.entity.BeersEntity
import kotlinx.coroutines.CoroutineScope


@Database(entities = [BeersEntity::class], version = 1)
abstract class DataBaseBeers: RoomDatabase(){

    abstract fun beersDao(): BeersDao?

    companion object {
        @Volatile
        private var INSTANCE: DataBaseBeers? = null
        @JvmStatic
        fun getDatabase(context: Context): DataBaseBeers? {
            if (INSTANCE == null) {
                synchronized(DataBaseBeers::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                DataBaseBeers::class.java, "beersData")
                                .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}