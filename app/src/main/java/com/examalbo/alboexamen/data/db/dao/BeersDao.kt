package com.examalbo.alboexamen.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.examalbo.alboexamen.data.db.entity.BeersEntity

@Dao
interface BeersDao {

    @Insert
     fun insert(beers: BeersEntity?)

    @get:Query("SELECT * FROM beers")
    val allBeers: LiveData<List<BeersEntity?>?>?


}