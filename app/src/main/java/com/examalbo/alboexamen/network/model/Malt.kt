package com.examalbo.alboexamen.network.model

data class Malt(
    val amount: AmountX,
    val name: String
)