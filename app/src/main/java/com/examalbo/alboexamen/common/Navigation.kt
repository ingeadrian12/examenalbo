package com.examalbo.alboexamen.common

import android.view.View
import com.examalbo.alboexamen.network.model.BeerResponseAItem

interface Navigation {
    fun nextScreen(onClickListener: View.OnClickListener, items: BeerResponseAItem)
}