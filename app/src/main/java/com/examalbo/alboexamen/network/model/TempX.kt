package com.examalbo.alboexamen.network.model

data class TempX(
    val unit: String,
    val value: Float
)