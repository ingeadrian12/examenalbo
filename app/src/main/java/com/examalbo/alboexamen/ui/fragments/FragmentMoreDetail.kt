package com.examalbo.alboexamen.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.examalbo.alboexamen.R
import com.examalbo.alboexamen.ui.viewmodels.FragmentMoreDetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_beers.*

class FragmentMoreDetail : Fragment() {

    private val picasso = Picasso.get()
    private var urlImage:String = ""
    private lateinit var nameBerrs: String
    private lateinit var tagline : String
    private lateinit var description: String
    private  lateinit var  firstbrewed : String
    private  lateinit var  foodpairing : List<String>
    companion object {
        fun newInstance() = FragmentMoreDetail()
    }

    private lateinit var viewModel: FragmentMoreDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_more_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FragmentMoreDetailViewModel::class.java)
        urlImage = arguments?.get("image") as String
        nameBerrs = arguments?.get("name") as String
        tagline = arguments?.get("Tagline") as String
        description = arguments?.get("Description") as String
        firstbrewed = arguments?.getString("Firstbrewed") as String
        foodpairing = arguments?.getStringArrayList("Foodpairing") as List<String>

        textView_Food_pairing.text = foodpairing.toString()
        nameBeersDetail.text = nameBerrs
        tagLineDetail.text = tagline
        textViewDate.text = firstbrewed
        textViewDescription.text = description

        setActivityTitle(nameBerrs)

        picasso.load(urlImage).into(imageViewDetail)

    }

    fun Fragment.setActivityTitle(title: String)
    {
        (activity as AppCompatActivity?)!!.supportActionBar?.title = title
    }

}