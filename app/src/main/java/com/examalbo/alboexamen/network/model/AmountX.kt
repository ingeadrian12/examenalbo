package com.examalbo.alboexamen.network.model

data class AmountX(
    val unit: String,
    val value: Double
)