package com.examalbo.alboexamen.network.model

data class Amount(
    val unit: String,
    val value: Float
)