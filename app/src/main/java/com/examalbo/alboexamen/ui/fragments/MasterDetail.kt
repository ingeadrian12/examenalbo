package com.examalbo.alboexamen.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.SparseArray
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.examalbo.alboexamen.R
import com.examalbo.alboexamen.common.Navigation
import com.examalbo.alboexamen.network.model.BeerResponse
import com.examalbo.alboexamen.network.model.BeerResponseAItem
import com.examalbo.alboexamen.ui.adapters.BeersAdapter
import com.examalbo.alboexamen.ui.viewmodels.MasterDetailViewModel
import kotlinx.android.synthetic.main.master_detail_fragment.*

class MasterDetail : Fragment(),Navigation {

    private lateinit var beersAdapter: BeersAdapter
    private   var index : Int = 10
    private var readyToRefresh = true
    private  var indexLast : Int =10
    private val mBeerList: MutableList<Any?> = mutableListOf()
    private var pagination: Int =0
    private val navigation by lazy { findNavController() }

    private val beersObserver = Observer<BeerResponse>(){
         getListBeers(it)
           readyToRefresh = (it.isNotEmpty() && it.size>=5)
           indexLast = it.size
           mBeerList.add(null)
           listBeers.adapter?.notifyItemInserted(mBeerList.lastIndex)
           mBeerList.remove(null)
           listBeers.adapter?.notifyItemRemoved(mBeerList.lastIndex)
           mBeerList.addAll(it)
           listBeers.adapter?.notifyDataSetChanged()


    }
    companion object {
        fun newInstance() = MasterDetail()
    }

    private lateinit var viewModel: MasterDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.master_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MasterDetailViewModel::class.java)
        init()
        viewModel.getBeeers(1,10)

        refresh.setOnRefreshListener {
            index += 1
            viewModel.getBeeers(1,index)
            refresh.isRefreshing = false
        }
        retrieveResponseServices()

        // TODO: Use the ViewModel
    }


    private fun retrieveResponseServices() {
        viewModel.listBeers.observe(viewLifecycleOwner, beersObserver)
    }
    private fun init(){
        pagination =1
        listBeers.apply {
            listBeers.layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
            addOnScrollListener(retrieveScrollListener(layoutManager as LinearLayoutManager))
        }

    }
    private  fun getListBeers(items: BeerResponse){
        beersAdapter = BeersAdapter(items,this)

        listBeers.adapter = beersAdapter

    }

    private fun retrieveScrollListener(layoutManager: LinearLayoutManager): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (readyToRefresh) {

                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            readyToRefresh = false
                          pagination++
                            viewModel.getBeeers(pagination,indexLast)
                            }

                        }
                    }
                }
            }
        }

    override fun nextScreen(onClickListener: View.OnClickListener, items: BeerResponseAItem) {

        val bundle = bundleOf("name" to items.name,"image" to items.image_url,"Tagline" to items.tagline
        ,"Description" to items.description,"Firstbrewed" to items.first_brewed,"Foodpairing" to items.food_pairing)


        navigation.navigate(R.id.action_masterDetail_to_fragmentMoreDetail,bundle)

    }

    fun Fragment.setActivityTitle(title: String)
    {
        (activity as AppCompatActivity?)!!.supportActionBar?.title = title
    }

    override fun onResume() {
        super.onResume()
        setActivityTitle("Berrs")
    }
}