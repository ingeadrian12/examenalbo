package com.examalbo.alboexamen.network.networkmanager

import com.examalbo.alboexamen.network.model.BeerResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Services {
    @GET("v2/beers")
    suspend fun getBeers(@Query("page") page: Int,@Query("per_page") per_page: Int):Response<BeerResponse>
}