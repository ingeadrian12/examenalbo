package com.examalbo.alboexamen.network.model

data class MashTemp(
    val duration: Float,
    val temp: TempX
)