package com.examalbo.alboexamen.common

import androidx.lifecycle.ViewModel

open class BaseModel: ViewModel() {
    private val repository by lazy { BaseRepository() }
}