package com.examalbo.alboexamen.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import com.examalbo.alboexamen.common.BaseModel
import com.examalbo.alboexamen.data.BeersRepository
import com.examalbo.alboexamen.data.db.entity.BeersEntity
import com.examalbo.alboexamen.network.model.BeerResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MasterDetailViewModel : BaseModel() {
    private val mRepository by lazy { BeersRepository() }
    val listBeers = MutableLiveData<BeerResponse>()

    fun getBeeers(page: Int, per_page: Int){

        GlobalScope.launch {
            val beers = mRepository.getBeersAll(page,per_page)

            withContext(Dispatchers.Main){
                listBeers.value = beers
                mRepository.getAllBeers()
                // insert(BeersEntity(beers))
                for ((index,value) in beers.withIndex()){
                    mRepository.insert(BeersEntity(value.name,value.tagline,value.image_url))
                }


            }
        }

    }
}