package com.examalbo.alboexamen.network.model

data class BoilVolume(
    val unit: String,
    val value: Float
)