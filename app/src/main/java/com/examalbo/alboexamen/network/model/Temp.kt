package com.examalbo.alboexamen.network.model

data class Temp(
    val unit: String,
    val value: Float
)